Le tutoriel du langage IDL est disponnible dans le [README du transpileur](https://gitlab.com/sharpattack/narniaddsgen/-/blob/main/README.md)

# IDL types to C mapping

| IDL                  | C             |
|----------------------|---------------|
| `float`              | `float`       |
| `double`             | `double`      |
| `short`              | `int16_t`     |
| `long`               | `int32_t`     |
| `long long`          | `int64_t`     |
| `unsigned short`     | `uint16_t`    |
| `unsigned long`      | `uint32_t`    |
| `unsigned long long` | `uint64_t`    |
| `char`               | `char`        |
| `boolean`            | `bool`        |
| `octet`              | `uint8_t`     |
| `string`             | `char[255]`   |
